#ifndef HASHDIALOG_H
#define HASHDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class HashDialog;
}

class HashDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HashDialog(QWidget *parent = 0);
    ~HashDialog();

private:
    Ui::HashDialog *ui;

public:
    void setHash(QString hash);

private slots:
    void copyToClipboard();
};

#endif // HASHDIALOG_H
