#include "hashdialog.h"
#include "ui_hashdialog.h"
#include <QClipboard>

HashDialog::HashDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HashDialog)
{
    ui->setupUi(this);

    //Connecting clicked signal to clipboard method
    connect(ui -> clipboardBtn, &QPushButton::clicked, this, &HashDialog::copyToClipboard);
}

HashDialog::~HashDialog()
{
    delete ui;
}


void HashDialog::copyToClipboard()
{
    QClipboard *clipBoard = QApplication::clipboard();

    clipBoard ->setText(ui -> hashTxtBrowser -> toPlainText());
}


void HashDialog::setHash(QString hash)
{
    ui -> hashTxtBrowser->setText(hash);
}
