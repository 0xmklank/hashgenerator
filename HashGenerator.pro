#-------------------------------------------------
#
# Project created by QtCreator 2016-11-30T17:13:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HashGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hashdialog.cpp

HEADERS  += mainwindow.h \
    hashdialog.h

FORMS    += mainwindow.ui \
    hashdialog.ui
