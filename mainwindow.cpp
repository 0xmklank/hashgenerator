#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hashdialog.h"

//Qt libraries
#include <QCryptographicHash>

//C++ standard libraries
#include <iostream>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui-> generateHashButton, &QPushButton::clicked, this, &MainWindow::generateHash);

    //Disable text controls when file is selected
    connect(ui -> fileRadioButton, &QRadioButton::clicked, this, &MainWindow::disableText);

    //Disable file controles when text is selected
    connect(ui -> textRadioButton, &QRadioButton::clicked, this, &MainWindow::disableFile);


    ui -> fileLabel -> setEnabled(false);
    ui -> fileButton -> setEnabled(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::generateHash()
{
    //Default hash code
    QCryptographicHash::Algorithm hashCode = QCryptographicHash::Md5;

    //QByteArray to hold the data and hash
    QByteArray hash;
    QString result;


    //Check which radio button is checked
    if(ui -> md5radio -> isChecked())
        //Setting the hashing code
        hashCode = QCryptographicHash::Md5;
    else if(ui -> sha256radio -> isChecked())
        //Setting the hashing code
        hashCode = QCryptographicHash::Sha256;
    else if(ui -> sha512radio -> isChecked())
        //Setting the hashing code
        hashCode = QCryptographicHash::Sha512;

    //Intialise the Crypto class with the chosen algorithm
    QCryptographicHash crypto(hashCode);

    //Check if text was supplied
    if(!ui -> textLineEdit ->text().isEmpty())
    {
        crypto.addData(ui -> textLineEdit->text().toUtf8());
        hash = crypto.result();
        result = hash.toHex();
    }
    else
        result = "No data was supplied";

    HashDialog *hashDlg = new HashDialog(this);
    hashDlg -> setModal(false);

    //Passing the result to the dialog to display it
    hashDlg -> setHash(result);
    hashDlg -> exec();
    hashDlg -> deleteLater();
}

void MainWindow::disableText()
{
    ui -> textLabel -> setEnabled(false);
    ui -> textLineEdit -> setEnabled(false);
    ui -> fileLabel -> setEnabled(true);
    ui -> fileButton -> setEnabled(true);
}

void MainWindow::disableFile()
{
    ui -> textLabel -> setEnabled(true);
    ui -> textLineEdit -> setEnabled(true);
    ui -> fileLabel -> setEnabled(false);
    ui -> fileButton -> setEnabled(false);
}
